# standard imports
import logging
import os
import unittest

# local imports
from yaml_acl.check import YAMLAcl
from yaml_acl.parse import YAMLCredentials

script_dir = os.path.dirname(os.path.realpath(__file__))

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class TestCheck(unittest.TestCase):

    acl = None
    credentials = None

    def setUp(self):
        datadir = os.path.join(script_dir, 'testdata')

        f = open(os.path.join(datadir, 'acl.yml'))
        d = f.read()
        f.close()
        TestCheck.acl = YAMLAcl(d)

        f = open(os.path.join(datadir, 'credentials.yml'))
        d = f.read()
        f.close()
        TestCheck.credentials = YAMLCredentials(d)

    def tearDown(self):
        pass


    def test_check(self):
        r = self.acl.check(self.credentials, '/foo', 'read')
        self.assertTrue(r)
        r = self.acl.check(self.credentials, '/foo', 'write')
        self.assertFalse(r)
        r = self.acl.check(self.credentials, '/foos', 'read')
        self.assertFalse(r)

        r = self.acl.check(self.credentials, '/bar', 'read')
        self.assertFalse(r)
        r = self.acl.check(self.credentials, '/bar', 'write')
        self.assertTrue(r)


if __name__ == '__main__':
    unittest.main()
